package com.epam.arrays.DroidQueueExample;

public class Droid {
    private String name;
    private int energyLevel;

    Droid(String name, int energyLevel) {
        this.name = name;
        this.energyLevel = energyLevel;
    }

    public String getName() {
        return name;
    }

    public int getEnergyLevel() {
        return energyLevel;
    }
}