package com.epam.arrays.DroidQueueExample;

import com.epam.arrays.ArraysSorting;
import com.epam.arrays.DroidQueueExample.Droid;

import java.util.*;

public class PriorityQueueExample {
    public static Comparator<Droid> energyComparator = new Comparator<Droid>() {
        @Override
        public int compare(Droid d1, Droid d2) {
            if (d1.getEnergyLevel() > d2.getEnergyLevel()) {
                return -1;
            }
            if (d1.getEnergyLevel() < d2.getEnergyLevel()) {
                return 1;
            }
            return 0;
        }
    };
    public static void main(String[] args) {
        Queue<Droid> droidPriorityQueue = new PriorityQueue<>(4, energyComparator);
        droidPriorityQueue.add(new Droid("Gon",2));
        droidPriorityQueue.add(new Droid("Master", 25));
        droidPriorityQueue.add(new Droid("Ion", 988));
        droidPriorityQueue.add(new Droid("Mystery", 1));
        droidPriorityQueue.add(new Droid("Mystery", 5));
        while (!droidPriorityQueue.isEmpty()) {
            System.out.println(droidPriorityQueue.poll().getName());
        }
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        ArraysSorting a = new ArraysSorting();

    }
}