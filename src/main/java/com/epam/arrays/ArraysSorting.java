package com.epam.arrays;
import java.util.*;

public class ArraysSorting {

    public Object[] getSameElements(Object[] a, Object[] b) {
        Object[] result = new Object[a.length + b.length];
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < b.length; j++)
                while (a[i] == b[j]) {
                    ArrayList p = new ArrayList();
                    p.add(a[i]);
                    result = p.toArray();
                    System.out.println(Arrays.toString(result));
                    break;
                }
        return result;
    }

    public Object[] getUnikalElements(Object[] a, Object[] b) {
        a = Arrays.stream(a).distinct().toArray();
        b = Arrays.stream(b).distinct().toArray();
        Object[] result = Arrays.copyOf(a, a.length + b.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        result = Arrays.stream(result).distinct().toArray();
        System.out.println(Arrays.toString(result));
        return result;

    }

}