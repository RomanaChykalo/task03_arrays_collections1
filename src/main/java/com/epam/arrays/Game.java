package com.epam.arrays;

import java.util.*;

public class Game {

    private int heroPower = 25;

    public class Artifact {
        int minPower = 10;
        int maxPower = 80;

        public int createPower(int min, int max) {
            int result = number.nextInt(max + 1 - min) + min;
            return +result;
        }
    }

    public class Monster {
        int minPower = 5;
        int maxPower = 100;

        public int createPower(int min, int max) {
            int result = number.nextInt(max + 1 - min) + min;
            return -result;
        }
    }

    int[] doors = new int[10];
    Random number = new Random();

    public void setMeaningForDoors() {
        Game game = new Game();
        Game.Artifact artifact = game.new Artifact();
        Game.Monster monster = game.new Monster();
        for (int i = 0; i < doors.length; i++) {
            double rand = Math.random();
            if (rand > 0.5) {
                doors[i] = monster.createPower(monster.minPower, monster.maxPower);
            } else {
                doors[i] = artifact.createPower(artifact.minPower, artifact.maxPower);
            }
        }
        System.out.println("Behind each door the hero can find positive or negative news: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] > 0) {
                System.out.println("If you opens door  " + (i + 1) + " your power will increase on " + doors[i]);
            } else {
                System.out.println("If you opens door  " + (i + 1) + " you must fight with monster!!!");
            }
        }
    }

    public int countDoorsOfDeath() {
        int doorsOfDeath = 0;
        for (int i = 0; i < doors.length; i++) {
            int resultOfDoorOpen = doors[i] + heroPower;
            if (resultOfDoorOpen < 0) {
                doorsOfDeath = doorsOfDeath + 1;
            }
        }
        System.out.print(doorsOfDeath);
        return doorsOfDeath;
    }

    public int[] createSequenceOfDoorsForHeroLiving() {
        int[] order = new int[doors.length];
        for (int variable = 0; variable < order.length; variable++) {
            order[variable] = variable;
        }
        for (int i = 0; i < doors.length; i++) {
            for (int j = doors.length - 1; j > 0; j--) {
                if (doors[j] > doors[j - 1]) {
                    int value = doors[j];
                    doors[j] = doors[j - 1];
                    doors[j - 1] = value;
                    int v = order[j];
                    order[j] = order[j - 1];
                    order[j - 1] = v;
                }

            }
        }
        return order;

    }


}
